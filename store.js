/**
 * Created by hanni on 11/4/16.
 */

function pass(arr, obj) {
     var rezult = -1;
    var strong = 0;
    var verif = 0;
    var keys = Object.keys(obj);

    arr.forEach( function (prodValue, index) {
        keys.forEach( function (keyValue) {
            if (prodValue[keyValue] == obj[keyValue]){
                verif += 1;
            }
        });

        if (verif == keys.length && strong == 0){
            rezult = index;
            strong++;
        }
        verif = 0;
    });
    return rezult;
}

function Store() {
   this.product = [];
    
    this.add = function (obj) {
        this.product.push(obj);
    }

    this.fetchAll = function () {
        return this.product;
    }

    this.fetch = function (obj) {

        return this.product[pass(this.product, obj)];
    }

    this.update = function (obj, updatedObj) {
        var keys = Object.keys(updatedObj);
        var index = 0;

        index = pass(this.product, obj);

        for (var i = 0; i < keys.length; i++) {
            this.product[index][keys[i]] = updatedObj[keys[i]];
        }
        return null;
    }
    
    this.delete = function (obj) {
        var index = 0;

        index = pass(this.product, obj);

        this.product = this.product.filter(function (value, i) {
                return i == index ? false : true;
        });
        return null;
    }
}


var obj1 = {id: 1, name: 'vas1', text: 'hi1'};
var obj2 = {id: 2, name: 'vas2', text: 'hi2'};
var obj3 = {id: 3, name: 'vas3', text: 'hi3'};
var obj4 = {id: 4, name: 'vas4', text: 'hi4'};
var obj5 = {id: 5, name: 'vas5', text: 'hi5'};
var obj6 = {id: 6, name: 'vas6', text: 'hi6'};
var obj7 = {id: 7, name: 'vas7', text: 'hi7'};
var obj8 = {id: 8, name: 'vas8', text: 'hi8'};

var obj9 = {id: 3, name: 'vas3'};

var obj10 = {id: 2, name: 'vas2'};


var store = new Store();

store.add(obj1);
store.add(obj2);
store.add(obj3);
store.add(obj4);


console.log(store.fetchAll());
console.log();
console.log(store.fetch( obj10 ));
store.update( obj10, {text: 'lahand',o:'pizdec'} );
console.log(store.fetchAll());
console.log(store.delete( obj9 ));
console.log(store.fetchAll());